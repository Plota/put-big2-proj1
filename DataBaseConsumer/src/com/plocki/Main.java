package com.plocki;
import java.sql.*;

public class Main {

    static String user = "root";
    static String userPwd = "root";
    static String jdbcHostname = "10.93.48.3";
    static String jdbcPort = "3306";
    static String jdbcDatabase = "proj1";

    static String jdbcUrl = "jdbc:mysql://" +jdbcHostname +":" +jdbcPort +"/" +jdbcDatabase;


    public static void main(String[] args) {
        try{

            Class.forName("com.mysql.jdbc.Driver");
            Connection con=DriverManager.getConnection(jdbcUrl,user,userPwd);



            if(args.length == 0){
                System.out.println("Missing argument \n RP - real picture \n AN - anomalies \n TURN - Turncate Table");
            }
            if(args[0].equals("RP")){
                    Statement stmt=con.createStatement();
                    printRealPicture(stmt);
                }
            if (args[0].equals("AN")){
                Statement stmt=con.createStatement();
                printAnomalies(stmt);
            }
            if (args[0].equals("TURNCATE")){
                Statement stmt=con.createStatement();
                stmt.executeUpdate("TRUNCATE TABLE proj1.realPicture");
                stmt.executeUpdate("TRUNCATE TABLE proj1.anomalies");
                System.out.println("Databese turncated");
            }







            con.close();
        }catch(Exception e){ System.out.println(e);}

    }


    static private void printRealPicture(Statement stmt){
         try {
             ResultSet rs=stmt.executeQuery("Select * from proj1.realPicture;");
             String realPictureFormat = "| %-10s | %-20s | %-11d | %-13d | %-23d | %-25d |%n";
             System.out.format("+------------+----------------------+-------------+---------------+-------------------------+---------------------------+%n");
             System.out.format("| Day        | Borough              | Arrivals_NO | Departures_NO | Arrival_Passenger_Count | Departure_Passenger_Count |%n");
             System.out.format("+------------+----------------------+-------------+---------------+-------------------------+---------------------------+%n");
             while(rs.next()) {
                 System.out.format(realPictureFormat,
                         rs.getString(1),
                         rs.getString(2),
                         rs.getInt(3),
                         rs.getInt(4),
                         rs.getInt(5),
                         rs.getInt(6)
                 );
             }
             System.out.format("+------------+----------------------+-------------+---------------+-------------------------+---------------------------+%n");
         }catch(Exception e){ System.out.println(e);}

     }


    static private void printAnomalies(Statement stmt){
        try {
            ResultSet rs =stmt.executeQuery("Select * from proj1.anomalies;");
            String anomaliesFormat = "| %-19s | %-19s | %-20s| %-23d | %-25d | %-12d |%n";
            System.out.format("+---------------------+---------------------+---------------------+-------------------------+---------------------------+--------------+%n");
            System.out.format("| start               | end                 |        Borough      | Arrival_Passenger_Count | Departure_Passenger_Count |  Diffrence   |%n");
            System.out.format("+---------------------+---------------------+---------------------+-------------------------+---------------------------+--------------+%n");
            while(rs.next()) {
                System.out.format(anomaliesFormat,
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        rs.getInt(6)
                );

            }
            System.out.format("+---------------------+---------------------+---------------------+-------------------------+---------------------------+--------------+%n");
        }catch(Exception e){ System.out.println(e);}
     }

}
