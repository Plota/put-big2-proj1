
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;


public class TestProducer {

    public static void main(String[] args) {

        String[] params = new String[]{
                "kafka-input", //directory - 0
                "10", //sleepTime - 1
                "kafka-tt-03-11", //topicName -2
                "1", //headerLength -3
                "plocki-bd2-w-0:9092"}; //bootstrapServers -4


        Properties props = new Properties();
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("directory",params[0]);
        props.put("sleepTime",params[1]);
        props.put("topicName",params[2]);
        props.put("headerLength",params[3]);
        props.put("bootstrap.servers", params[4]);

        KafkaProducer<String, String> producer = new KafkaProducer<>(props);

        final File folder = new File(params[0]);
        File[] listOfFiles = folder.listFiles();
        String listOfPaths[] = Arrays.stream(listOfFiles).
                map(file -> file.getAbsolutePath()).toArray(String[]::new);
        Arrays.sort(listOfPaths);
        for (final String fileName : listOfPaths) {
            try (Stream<String> stream = Files.lines(Paths.get(fileName)).
                    skip(Integer.parseInt(params[3]))) {

                stream.forEach(line -> producer.send(new ProducerRecord<String, String>(
                        params[2],String.valueOf(line.hashCode()),line
                )));
                TimeUnit.SECONDS.sleep(Integer.parseInt(params[1]));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        producer.close();
    }

    }
