CLUSTER_NAME=$(/usr/share/google/get_metadata_value attributes/dataproc-cluster-name)
cd /usr/lib/kafka

bin/kafka-topics.sh --create --zookeeper ${CLUSTER_NAME}-m:2181 \
 --replication-factor 1 \
 --partitions 1 --topic kafka-to-ss


cd $home
CLUSTER_NAME=$(/usr/share/google/get_metadata_value attributes/dataproc-cluster-name)
echo "Start"
sudo java -cp /usr/lib/kafka/libs/*:Producent.jar \
com.example.bigdata.TestProducer /usr/lib/kafka/tmp/taxi/yellow_tripdata_result 15 \
kafka-to-ss 1 ${CLUSTER_NAME}-w-0:9092
