package TaxiProject

import java.util.Properties

import org.apache.spark.sql.streaming._
import org.apache.spark.sql.functions.split
import org.apache.spark.sql.functions.{count, to_date, to_timestamp, _}
import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, SparkSession}

object Main extends App {


  //-----------------------------SET UP SPARK--------------------------------------------
  val conf = new SparkConf().setAppName("SparkDemo")
  val spark = SparkSession.builder
    .config(conf)
    .getOrCreate()

  import spark.implicits._

  spark.sparkContext.setLogLevel("ERROR")
  spark.conf.set("spark.sql.session.timeZone", "UTC")


  //----------------------------SET UP DATABASE CONNECTION-------------------------------
  val user = "root"
  val userPwd = "root"
  val jdbcHostname = "10.93.48.3"
  val jdbcPort = 3306
  val jdbcDatabase = "proj1"

  val jdbcUrl = s"jdbc:mysql://${jdbcHostname}:${jdbcPort}/${jdbcDatabase}"


  val connectionProperties = new Properties()
  connectionProperties.put("user", user)
  connectionProperties.put("password", userPwd)

  val D = args(0).toInt
  val P = args(1).toInt
  val hours = D + " hours"



  //----------------------------------LOAD STATIC TABLE------------------

  val myLookupFile = spark.read
    .option("sep", ",")
    .format("csv")
    .option("header","true")
    .load("gs://put-big-data-storage/lookup.csv")


  val lookups = myLookupFile.map(row => Lookup(Integer.valueOf(row.getString(0)),row.getString(1),row.getString(2),row.getString(3)))
  lookups.show(10)



  //-------------------------------READ FROM KAFKA--------------------------------------------------------------
  val stream = spark
    .readStream
    .format("kafka")
    .option("kafka.bootstrap.servers", "put-big-data-cluster-w-0:9092")
    .option("subscribe", "kafka-to-ss")
    .load()

  val castedStream = stream
    .selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
    .as[(String, String)]

  val values = castedStream.select("value")

  val splited =values.withColumn("_tmp", split($"value", ",")).select(
    $"_tmp".getItem(0).as("tripID"),
    $"_tmp".getItem(1).as("start_stop"),
    $"_tmp".getItem(2).as("timestamp"),
    $"_tmp".getItem(3).as("locationID"),
    $"_tmp".getItem(4).as("passenger_count"),
    $"_tmp".getItem(5).as("trip_distance"),
    $"_tmp".getItem(6).as("payment_type"),
    $"_tmp".getItem(7).as("amount"),
    $"_tmp".getItem(8).as("VendorID")
  )

  val timestampFixed = splited.withColumn("timestamp", to_timestamp($"timestamp","yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"))

  val trips = timestampFixed.map(row => EventTrip(
    row.getString(0).toInt,
    row.getString(1).toInt,
    row.getTimestamp(2),
    row.getString(3).toInt,
    row.getString(4).toInt,
    row.getString(5).toDouble,
    row.getString(6).toInt,
    row.getString(7).toDouble,
    row.getString(8).toInt
  ))

  val joined = trips
    .join(lookups, trips("locationID") === lookups("LocationID"))
    .select($"tripID",$"start_stop",$"timestamp",$"passenger_count",$"Borough")




  //--------------------------------------------AGGREGATE DATA-------------------------------------
  val groupRealPicture = joined
    .withWatermark("timestamp","5 minutes")
    .groupBy(window($"timestamp","24 hour","24 hour").as("Day"),$"Borough")
    .agg(
      count(when($"start_stop" === 0,$"tripID")).as("Arrivals_NO"),
      count(when($"start_stop" === 1,$"tripID")).as("Departures_NO"),
      sum(when($"start_stop" === 0,$"passenger_count")).as("Arrival_Passenger_Count"),
      sum(when($"start_stop" === 1,$"passenger_count")).as("Departure_Passenger_Count")
    )

  val realPicture = groupRealPicture.
    withColumn("Day",to_date($"Day.start")).
    select("Day","Borough","Arrivals_NO","Departures_NO","Arrival_Passenger_Count","Departure_Passenger_Count")


  //----------------------------------------------DEDECT ANOMALIES----------------------------------------
  val group = joined
    .withWatermark("timestamp","5 minutes")
    .groupBy(window($"timestamp",hours, "1 hour"),$"Borough")
    .agg(
      count(when($"start_stop" === 0,$"tripID")).as("Arrivals NO"),
      count(when($"start_stop" === 1,$"tripID")).as("Departures NO"),
      sum(when($"start_stop" === 0,$"passenger_count")).as("Arrival_Passenger_Count"),
      sum(when($"start_stop" === 1,$"passenger_count")).as("Departure_Passenger_Count")
    )

  val filtered = group
    .withColumn("starting",hour($"window.start"))
    .filter($"starting"<=(24-D))

  val anomalies = filtered
    .withColumn("Diffrence", group("Arrival_Passenger_Count") - group("Departure_Passenger_Count"))
    .filter($"Diffrence"< -P)
    .select(
      $"window.start",
      $"window.end",
      $"Borough",
      $"Arrival_Passenger_Count",
      $"Departure_Passenger_Count",
      $"Diffrence"
    )



  //--------------------------------WRITE STREAM------------------------------------------------------------------------


  anomalies.writeStream.
    trigger(ProcessingTime("1 hour")).
    format("console").
    queryName("anomalies-query").
    foreachBatch({
      (batchDF: DataFrame, batchId: Long) =>
        batchDF.
          write.
          mode("append").
          jdbc(jdbcUrl, "anomalies", connectionProperties)
    }).
    start()

  realPicture.writeStream.
    trigger(ProcessingTime("1 hour")).
    format("console").
    queryName("realPicture-query").
    foreachBatch({
      (batchDF: DataFrame, batchId: Long) =>
        batchDF.
          write.
          mode("append").
          jdbc(jdbcUrl, "realPicture", connectionProperties)
    }).
    start()

  spark.streams.awaitAnyTermination
}

