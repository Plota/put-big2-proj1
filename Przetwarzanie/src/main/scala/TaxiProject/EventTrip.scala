package TaxiProject

import java.sql.Timestamp


case class EventTrip (
                       tripID : Int,
                       start_stop: Int,
                       timestamp: Timestamp,
                       locationID: Int,
                       passenger_count: Int,
                       trip_distance: Double,
                       payment_type: Int,
                       amount: Double,
                       VendorID: Int
                     )
